<?php
namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\View;
use App\Core\Session;

/**
 *  Dashboard
 */
class Dashboard extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{
		//Check if Logged in is true
		Session::init();
		$auth = Session::get('auth');
		if ($auth == false) {
			Session::destroy();
			header('Location: ../../Home/index');
			return false;
		}
		else 
			return true;
	}

	public function indexAction( $args = array() )
	{

		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = 'Dashboard';

		Session::init();
		$auth = Session::get('auth');
		$input = array( 'auth' => $auth );

		$data = array_merge($scVariables, $input);

        View::render('b1', 'dashboard', $data);
	}    

    protected function after()
	{

	}


} //END CLASS