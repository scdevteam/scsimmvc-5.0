<?php
namespace App\Controllers\Dev;
use App\Core\Controller;
use App\Core\View;
/**
 *  Paths
 */
class Phpinfo extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function IndexAction()
	{
			View::render('d0', 'phpinfo');
	}

    protected function after()
	{
		
	}

} //END CLASS