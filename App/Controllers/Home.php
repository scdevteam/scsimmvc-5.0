<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Home
 */
class Home extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function indexAction( $args = array() )
	{
		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = 'Home';

		//input
		$input = array( 'name' => 'kribo');

		$data = array_merge($scVariables, $input);

		View::renderTemplate('home.html', $data);

	}

    protected function after()
	{

	}

} //END CLASS